package com.example.lemonadsdelanizlemona.service;

import com.example.lemonadsdelanizlemona.dto.response.UserDtoResponse;
import com.example.lemonadsdelanizlemona.exception.domain.LoginExistException;
import com.example.lemonadsdelanizlemona.exception.domain.UserNotFoundException;
import com.example.lemonadsdelanizlemona.model.Auth;
import com.example.lemonadsdelanizlemona.model.User;
import com.example.lemonadsdelanizlemona.repository.AuthRepository;
import com.example.lemonadsdelanizlemona.repository.UserRepository;
import com.example.lemonadsdelanizlemona.utils.facade.UserFacade;
import org.springframework.stereotype.Service;
import java.util.Optional;
import java.util.UUID;

@Service
public class AuthService {
    private final AuthRepository accountRepository;
    private final UserRepository userRepository;

    public AuthService(AuthRepository accountRepository, UserRepository userRepository) {
        this.accountRepository = accountRepository;
        this.userRepository = userRepository;
    }

    public UserDtoResponse register(String login, String password){
        Optional<Auth> accountOptimal = accountRepository.findAuthByUsername(login);
        if(accountOptimal.isPresent()){
            throw new LoginExistException("This login is already exists");
        } else{
            Auth account = new Auth();
            User user = new User();
            User newUser = userRepository.save(user);

            account.setUsername(login);
            account.setPassword(password);
            account.setToken(UUID.randomUUID().toString());
            account.setUser(newUser);

            Auth newAccount = accountRepository.save(account);

            return new UserFacade().accountToUserDtoResponse(newAccount);
        }
    }

    public String authenticate(String login, String password) {
        Optional<Auth> accountOptional = accountRepository.findAuthByUsernameAndPassword(login,password);
        if(accountOptional.isPresent()){
            return accountOptional.get().getToken();
        } else{
            throw new UserNotFoundException("Invalid login or password");
        }
    }


}
