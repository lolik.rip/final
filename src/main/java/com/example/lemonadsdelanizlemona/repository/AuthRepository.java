package com.example.lemonadsdelanizlemona.repository;

import com.example.lemonadsdelanizlemona.model.Auth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthRepository extends JpaRepository<Auth, Long> {
    Optional<Auth> findAuthByUsername(String username);
    Optional<Auth> findAuthByUsernameAndPassword(String username, String password);

}
