package com.example.lemonadsdelanizlemona.dto.request;

import lombok.Data;

@Data
public class AuthRequest {
    private String name;
    private String login;
    private String password;
}
