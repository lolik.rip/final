package com.example.lemonadsdelanizlemona.dto.response;

import lombok.Data;

@Data
public class UserDtoResponse {
    private Long id;
    private String login;
    private String name;
}
