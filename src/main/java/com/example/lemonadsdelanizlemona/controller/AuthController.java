package com.example.lemonadsdelanizlemona.controller;

import com.example.lemonadsdelanizlemona.dto.request.AuthRequest;
import com.example.lemonadsdelanizlemona.dto.response.UserDtoResponse;
import com.example.lemonadsdelanizlemona.exception.ExceptionHandling;
import com.example.lemonadsdelanizlemona.service.AuthService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import static org.springframework.http.HttpStatus.OK;

@Controller
public class AuthController extends ExceptionHandling {
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping({"/register"})
    public ResponseEntity<Object> register(@RequestBody AuthRequest request){
        UserDtoResponse userDtoResponse = authService.register(request.getLogin(), request.getPassword());
        return new ResponseEntity<>(userDtoResponse, OK);
    }

    @PostMapping({"/login"})
    public  ResponseEntity<Object> login(@RequestBody AuthRequest request){
        String token = authService.authenticate(request.getLogin(), request.getPassword());
        return new ResponseEntity<>(token, OK);
    }

}
