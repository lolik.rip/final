package com.example.lemonadsdelanizlemona.exception.domain;

public class TokenIsNotValidException extends RuntimeException {
    public TokenIsNotValidException(String message) {
        super(message);
    }
}
