package com.example.lemonadsdelanizlemona.exception.domain;

public class LoginExistException extends RuntimeException {
    public LoginExistException(String message) {
        super(message);
    }
}
