package com.example.lemonadsdelanizlemona.utils.facade;

import com.example.lemonadsdelanizlemona.dto.response.UserDtoResponse;
import com.example.lemonadsdelanizlemona.model.Auth;

public class UserFacade {
    public UserDtoResponse accountToUserDtoResponse(Auth account) {
        UserDtoResponse userDtoResponse = new UserDtoResponse();
        userDtoResponse.setId(account.getId());
        userDtoResponse.setName(account.getUser().getName());
        userDtoResponse.setLogin(account.getUsername());
        return userDtoResponse;
    }
}
