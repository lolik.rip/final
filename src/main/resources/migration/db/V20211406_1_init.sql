drop table if exists users;
create table users
(
    id   bigint serial not null
        constraint users_pkey
            primary key,
    name varchar(255) not null
);

drop table if exists auth;
create table auth
(
    id   bigint serial   not null
        constraint auth_pkey
            primary key,
    password varchar(255) not null,
    username varchar(255) not null
        constraint uk_5re8bghl4sqkib3pcehhi4src
            unique,
    user_id  bigint       not null
        constraint uk_ox9lr2lxr8h7undhmflx4xqky
            unique
        constraint fkpv45mvdt7km1gvrtn5f9rsvd7
            references users
);

drop table if exists chats;
create table chats
(
    id   bigint serial    not null
        constraint chats_pkey
            primary key,
    name varchar(255) not null
);

drop table if exists messages;
create table messages
(
    id   bigint serial not null
        constraint messages_pkey
            primary key,
    text    varchar(255),
    chad_id bigint    not null
        constraint fkhs50j8ypqa96u11q74849dyem
            references chats,
    user_id bigint    not null
        constraint fkpsmh6clh3csorw43eaodlqvkn
            references users
);



